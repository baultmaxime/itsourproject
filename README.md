# Qui ?
Une entreprise de 10 personnes réparties dans plusieurs services : Salariées , Techniciens , Administrateur

# Qui souhaitent ?
Une plateforme pour traiter les demandes informatiques/incidents informatiques grace a un logiciel de ticketing
<br>les salariées souhaitent ouvrir des incidents et suivre l'avancée de la resolution
<br>le technicien veut resoudre l'incident du salariées
<br>l'administrateur veut donner les droit et administrer glpi

# Notre produit est ? 
Notre produit est un logiciel libre de gestion des services informatiques et de gestion de parc informatiques :
- GLPI : logiciel libre de gestion de service informatique et de gestion de services d'assistances
- Active directory : service d'annuaire pour systeme d'exploitation Windows
- OCS : un logiciel qui permet de realiser un inventaire sur la configuration materielle des machines sur le reseau et logiciel installés

# A la difference ?
<br> A la difference de spicework notre logiciel gere la gestion des SLA 
<br> Elle integre la certification ITIL 
<br> Pas d'integration de mots de passe pour les utilisateurs 

# A la difference de notre concurent
<br> Notre logiciel ne contient aucune pub.
<br> Le logiciel glpi est completement traduit en français.

